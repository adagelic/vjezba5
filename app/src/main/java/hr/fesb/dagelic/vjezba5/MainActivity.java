package hr.fesb.dagelic.vjezba5;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.loopj.android.http.AsyncHttpClient;
import com.loopj.android.http.RequestParams;
import com.loopj.android.http.TextHttpResponseHandler;

import cz.msebera.android.httpclient.Header;

public class MainActivity extends AppCompatActivity {

    public static String apiUrl = "http://android.getbybus.net/chat-exercise/";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        final EditText nameEt = (EditText) findViewById(R.id.nameEditText);
        final EditText messageEt = (EditText) findViewById(R.id.messageContentEditText);
        Button sendBtn = (Button) findViewById(R.id.sendButton);

        final AsyncHttpClient myAsyncClient = new AsyncHttpClient();

        sendBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                RequestParams apiCallParams = new RequestParams();
                apiCallParams.add("action", "new");
                apiCallParams.add("user", nameEt.getText().toString());
                apiCallParams.add("message", messageEt.getText().toString());

                myAsyncClient.get(apiUrl, apiCallParams, new TextHttpResponseHandler() {
                    @Override
                    public void onFailure(int statusCode, Header[] headers, String responseString, Throwable throwable) {
                        Toast.makeText(getApplicationContext(), "Greska u komunikaciji sa serverom",
                                Toast.LENGTH_LONG).show();
                    }

                    @Override
                    public void onSuccess(int statusCode, Header[] headers, String responseString) {
                        Toast.makeText(getApplicationContext(), "Poruka uspjesno poslana",
                                Toast.LENGTH_LONG).show();
                    }
                });

            }
        });
    }
}
